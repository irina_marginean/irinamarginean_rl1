﻿using System.Windows;
using Caliburn.Micro;
using PresentationLayer.ViewModels;

[assembly: log4net.Config.XmlConfigurator(ConfigFile = "log4net.config", Watch = true)]
namespace PresentationLayer
{
    public class Bootstrapper : BootstrapperBase
    {
        private static readonly log4net.ILog Log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Bootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<ProductsDisplayViewModel>();
        }
    }
}
