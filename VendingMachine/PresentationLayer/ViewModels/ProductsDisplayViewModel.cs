﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using BusinessLogicLayer;
using BusinessLogicLayer.Helpers;
using Caliburn.Micro;
using DataAccessLayer.Models;

namespace PresentationLayer.ViewModels
{
    public class ProductsDisplayViewModel : Screen
    {
        private static readonly log4net.ILog Log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private decimal currentSelectedAmount = 0;

        public Product SelectedProduct{ get; set; }
        public string CardName { get; set; }
        public string CardNumber { get; set; }
        public string ExpiryDate { get; set; }
        public string CVV { get; set; }
        public string AmountToPay { get; set; } = "0";
        public string CurrentAmount { get; set; }

        public List<Money> CurrentMoneyList { get; set; } = new List<Money>
        {
            new Money
            {
                Name = "TenCents",
                Value = 0.1M,
                Quantity = 0
            },
            new Money
            {
                Name = "FiftyCents",
                Value = 0.5M,
                Quantity = 0
            },
            new Money
            {
                Name = "OneEuro",
                Value = 1,
                Quantity = 0
            },
            new Money
            {
                Name = "TwoEuros",
                Value = 2,
                Quantity = 0
            },
            new Money
            {
                Name = "FiveEuros",
                Value = 5,
                Quantity = 0
            },
            new Money
            {
                Name = "TenEuros",
                Value = 10,
                Quantity = 0
            }
        };

        public decimal CurrentSelectedAmount
        {
            get => currentSelectedAmount;
            set
            {
                currentSelectedAmount = value;
                CurrentAmount = currentSelectedAmount.ToString();
                OnPropertyChanged(nameof(CurrentAmount));
            }
        }


        public BindableCollection<Product> Products { get; set; }

        public ProductsDisplayViewModel()
        {
            Products = new BindableCollection<Product>(VendingMachineFacade.GetProducts());
        }

        public void SelectProduct()
        {
            try
            {
                VendingMachineFacade.ValidateProduct(SelectedProduct);

                AmountToPay = SelectedProduct.Price.ToString();
                OnPropertyChanged(nameof(AmountToPay));
            }
            catch (ValidationException productValidationException)
            {
                MessageBox.Show(
                    $"{productValidationException.Message} Try selecting another product!",
                    "Product Validation Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);

                Log.Error("The product that was selected is not valid!", productValidationException);
            }
            catch (NullReferenceException nullReferenceException)
            {
                MessageBox.Show(
                    "No product was selected!",
                    "Selection Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);

                Log.Error("No product was selected!", nullReferenceException);
            }
        }

        public void PayByCard()
        {
            try
            {
                IPaymentModel card = new CardPaymentModel
                {
                    CardHolderName = CardName,
                    CardNumber = CardNumber,
                    ExpirationMonth = Int32.Parse(ExpiryDate.Substring(0, 2)),
                    ExpirationYear = Int32.Parse($"20{ExpiryDate.Substring(3, 2)}"),
                    CVV = Int32.Parse(CVV)
                };

                VendingMachineFacade.MakePayment(card, SelectedProduct.Price);

                VendingMachineFacade.DecreaseProductQuantity(SelectedProduct);

                Transaction transaction = new Transaction
                {
                    Product = SelectedProduct,
                    Date = DateTime.Now,
                    PaymentType = "card"
                };

                VendingMachineFacade.LogTransaction(transaction);

                MessageBox.Show(
                    $"You bought {SelectedProduct.Name}",
                    "Thank you!",
                    MessageBoxButton.OK);

                Products.Refresh();

                SelectedProduct = null;
                AmountToPay = "0";
                OnPropertyChanged(nameof(AmountToPay));

                ResetCard();
            }
            catch (NullReferenceException nullReferenceException)
            {
                MessageBox.Show(
                    "No product/card was selected!",
                    "Selection Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);

                Log.Error("No product/card was selected!", nullReferenceException);
            }
            catch (FormatException e)
            {
                MessageBox.Show(
                    "Input is not valid!",
                    "Input format error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);

                Log.Error("Input is not valid!", e);
            }
            catch (ValidationException e)
            {
                MessageBox.Show(
                    e.Message,
                    "Selection Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);

                Log.Error("Card is not valid", e);
            }
            catch (Exception e)
            {
                MessageBox.Show(
                    "Error occurred",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);

                Log.Error(e.Message, e);
            }
        }

        public void SelectTenCents()
        {
            Money tenCents = CurrentMoneyList.Find(x => x.Name == "TenCents");

            CurrentSelectedAmount += tenCents.Value;

            tenCents.Quantity++;
        }

        public void SelectFiftyCents()
        {
            Money fiftyCents = CurrentMoneyList.Find(x => x.Name == "FiftyCents");

            CurrentSelectedAmount += fiftyCents.Value;

            fiftyCents.Quantity++;
        }

        public void SelectOneEuro()
        {
            Money oneEuro = CurrentMoneyList.Find(x => x.Name == "OneEuro");

            CurrentSelectedAmount += oneEuro.Value;

            oneEuro.Quantity++;
        }

        public void SelectTwoEuros()
        {
            Money twoEuros = CurrentMoneyList.Find(x => x.Name == "TwoEuros");

            CurrentSelectedAmount += twoEuros.Value;

            twoEuros.Quantity++;
        }

        public void SelectFiveEuros()
        {
            Money fiveEuros = CurrentMoneyList.Find(x => x.Name == "FiveEuros");

            CurrentSelectedAmount += fiveEuros.Value;

            fiveEuros.Quantity++;
        }

        public void SelectTenEuros()
        {
            Money tenEuros = CurrentMoneyList.Find(x => x.Name == "TenEuros");

            CurrentSelectedAmount += tenEuros.Value;

            tenEuros.Quantity++;
        }

        public void PayByCash()
        {
            try
            {
                IPaymentModel moneyPaymentModel = new MoneyPaymentModel
                {
                    InputMoney = CurrentMoneyList.AsEnumerable(),
                    Rest = new List<Money>()
                };

                VendingMachineFacade.MakePayment(moneyPaymentModel, SelectedProduct.Price);

                VendingMachineFacade.DecreaseProductQuantity(SelectedProduct);

                Transaction transaction = new Transaction
                {
                    Product = SelectedProduct,
                    Date = DateTime.Now,
                    PaymentType = "cash"
                };

                VendingMachineFacade.LogTransaction(transaction);

                MessageBox.Show(
                    $"You bought {SelectedProduct.Name}",
                    "Thank you!",
                    MessageBoxButton.OK);

                Products.Refresh();

                SelectedProduct = null;
                AmountToPay = "0";
                OnPropertyChanged(nameof(AmountToPay));

                ResetCash();
            }
            catch (NullReferenceException nullReferenceException)
            {
                MessageBox.Show(
                    "No product was selected!",
                    "Selection Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);

                Log.Error("No product was selected!", nullReferenceException);
            }
            catch (ValidationException e)
            {
                MessageBox.Show(
                    e.Message,
                    "Selection Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);

                Log.Error("Cash payment is not valid", e);
            }
        }

        public void ResetCash()
        {
            CurrentSelectedAmount = 0;

            foreach (Money money in CurrentMoneyList)
            {
                money.Quantity = 0;
            }
        }

        public void ResetCard()
        {
            CardName = null;
            CardNumber = null;
            ExpiryDate = null;
            CVV = null;

            OnPropertyChanged(nameof(CardName));
            OnPropertyChanged(nameof(CardNumber));
            OnPropertyChanged(nameof(ExpiryDate));
            OnPropertyChanged(nameof(CVV));
        }

        public void GenerateReports()
        {
            string reports = VendingMachineFacade.GenerateReports();

            MessageBox.Show(
                reports,
                "REPORTS",
                MessageBoxButton.OK);
        }

        public override event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
