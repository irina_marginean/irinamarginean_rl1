﻿using DataAccessLayer.Models;

namespace BusinessLogicLayer.PaymentService
{
    interface IPaymentStrategy
    {
        void MakePayment<T>(T model, decimal valueToPay) where T : IPaymentModel;
    }
}
