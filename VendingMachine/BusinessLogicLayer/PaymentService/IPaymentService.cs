﻿using System;
using DataAccessLayer.Models;

namespace BusinessLogicLayer.PaymentService
{
    public interface IPaymentService
    {
        void MakePayment<T>(T model, decimal valueToPay) where T : IPaymentModel;
        bool AppliesTo(Type provider);
    }
}
