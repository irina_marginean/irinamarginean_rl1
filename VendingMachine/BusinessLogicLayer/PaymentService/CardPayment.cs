﻿using System;
using System.ComponentModel.DataAnnotations;
using CreditCardValidator;
using DataAccessLayer.Models;

namespace BusinessLogicLayer.PaymentService
{
    public class CardPayment : AbstractPayment<CardPaymentModel>
    {
        protected override void Validate(CardPaymentModel model, decimal valueToPay)
        {
            CreditCardDetector creditCardDetector = new CreditCardDetector(model.CardNumber);

            if (!creditCardDetector.IsValid())
            {
                throw new ValidationException("Credit card number is not valid!");
            }

            if (model.CVV.ToString().Length != 3)
            {
                throw new ValidationException("CVV does not have 3 digits!");
            }

            if (model.ExpirationMonth <= 0 || model.ExpirationMonth > 12)
            {
                throw new ValidationException("The month must be between 1 and 12!");
            }

            DateTime expirationDate = new DateTime(model.ExpirationYear, model.ExpirationMonth, 1);

            if (DateTime.Now > expirationDate)
            {
                throw new ValidationException("Credit card is expired!");
            }
        }

        protected override void MakePayment(CardPaymentModel model, decimal valueToPay)
        {
            Validate(model, valueToPay);
        }
    }
}
