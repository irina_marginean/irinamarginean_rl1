﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccessLayer.Models;

namespace BusinessLogicLayer.PaymentService
{
    public class PaymentStrategy : IPaymentStrategy
    {
        private readonly IEnumerable<IPaymentService> paymentServices;

        public PaymentStrategy(IEnumerable<IPaymentService> paymentServices)
        {
            this.paymentServices = paymentServices;
        }

        public void MakePayment<T>(T model, decimal valueToPay) where T : IPaymentModel
        {
            GetPaymentService(model).MakePayment(model, valueToPay);
        }

        private IPaymentService GetPaymentService<T>(T model) where T : IPaymentModel
        {
            IPaymentService result = paymentServices.FirstOrDefault(x => x.AppliesTo(model.GetType()));

            if (result == null)
            {
                throw new InvalidOperationException(
                    $"Payment service for {model.GetType()} not registered.");
            }

            return result;
        }
    }
}
