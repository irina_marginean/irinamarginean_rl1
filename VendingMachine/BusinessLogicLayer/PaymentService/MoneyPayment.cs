﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using DataAccessLayer.Models;
using DataAccessLayer.Repositories;


namespace BusinessLogicLayer.PaymentService
{
    public class MoneyPayment : AbstractPayment<MoneyPaymentModel>
    {
        protected override void Validate(MoneyPaymentModel model, decimal valueToPay)
        {
            if (!CheckIfEnoughInputMoney(model.InputMoney, valueToPay))
            {
                throw new ValidationException("Not enough input money!");
            }
        }

        protected override void MakePayment(MoneyPaymentModel model, decimal valueToPay)
        {
           Validate(model, valueToPay);
           
           AddInputMoneyToDatabase(model.InputMoney);

           //model.Rest = ComputeRest(model.InputMoney, valueToPay);
        }

        private void AddInputMoneyToDatabase(IEnumerable<Money> inputMoney)
        {
            foreach (var money in inputMoney)
            {
                Money dbMoney = RepositoryLocator.MoneyRepository.GetAll().FirstOrDefault(x => x.Name == money.Name);

                if (dbMoney != null)
                {
                    dbMoney.Quantity += money.Quantity;

                    RepositoryLocator.MoneyRepository.Update(dbMoney);
                }
            }
        }

        private IEnumerable<Money> ComputeRest(IEnumerable<Money> inputMoney, decimal valueToPay)
        {
            decimal restDecimal = valueToPay - ConvertMoneyToDecimal(inputMoney);

            if (CheckIfEnoughRestInDatabase(restDecimal))
            {
                throw new ValidationException("Not enough rest in the vending machine!");
            }

            IEnumerable<Money> restMoney = new List<Money>();

            List<Money> moneyInDatabase = RepositoryLocator.MoneyRepository.GetAll().OrderBy(x => x.Value).Reverse().ToList();

            while (restDecimal > 0)
            {
                foreach (var money in moneyInDatabase)
                {
                    decimal currentValue = restDecimal / money.Value;

                    if (currentValue > 0)
                    {
                        restDecimal -= money.Value * currentValue;

                        Money dbMoney = RepositoryLocator.MoneyRepository.GetAll().FirstOrDefault(x => x.Name == money.Name);

                        dbMoney.Quantity -= money.Quantity;

                        RepositoryLocator.MoneyRepository.Update(dbMoney);
                    }
                }
            }

            return restMoney;
        }

        private bool CheckIfEnoughInputMoney(IEnumerable<Money> inputMoney, decimal valueToPay)
        {
            decimal inputValue = ConvertMoneyToDecimal(inputMoney);

            return (inputValue >= valueToPay);
        }

        private bool CheckIfEnoughRestInDatabase(decimal rest)
        {
            IEnumerable<Money> moneyInDatabase = RepositoryLocator.MoneyRepository.GetAll();

            decimal total = ConvertMoneyToDecimal(moneyInDatabase);

            return (rest <= total);
        }

        private decimal ConvertMoneyToDecimal(IEnumerable<Money> moneyEnumerable)
        {
            decimal total = 0;

            foreach (var money in moneyEnumerable)
            {
                total += money.Value * money.Quantity;
            }

            return total;
        }
    }
}
