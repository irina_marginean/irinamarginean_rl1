﻿using System;
using DataAccessLayer.Models;

namespace BusinessLogicLayer.PaymentService
{
    public abstract class AbstractPayment<TModel> : IPaymentService where TModel : IPaymentModel
    {
        public void MakePayment<T>(T model, decimal valueToPay) where T : IPaymentModel
        {
            MakePayment((TModel)(object)model, valueToPay);
        }

        public bool AppliesTo(Type provider)
        {
            return typeof(TModel) == provider;
        }

        protected abstract void Validate(TModel model, decimal valueToPay);

        protected abstract void MakePayment(TModel model, decimal valueToPay);
    }
}
