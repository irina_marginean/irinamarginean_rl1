﻿namespace BusinessLogicLayer
{
    public interface IValidate<T>
    {
        void Validate(T t);
    }
}
