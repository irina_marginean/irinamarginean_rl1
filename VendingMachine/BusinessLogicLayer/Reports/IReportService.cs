﻿using System.Collections.Generic;
using DataAccessLayer.Models;

namespace BusinessLogicLayer.Reports
{
    public interface IReportService
    {
        int GetTotalNumberOfSoldItems();
        Product GetMostExpensiveSoldItem();
        Product GetMostSoldItem();
        List<Product> GetLeastSoldItems();
    }
}
