﻿using System.Collections.Generic;
using System.Text;
using DataAccessLayer.Models;

namespace BusinessLogicLayer.Reports
{
    public class ReportsBuilder : IReportsBuilder
    {
        private readonly IReportService reportService;
        private readonly StringBuilder reports;

        public ReportsBuilder(IReportService reportService)
        {
            this.reportService = reportService;
            reports = new StringBuilder();
        }

        public IReportsBuilder AddMostSoldItemReport()
        {
            Product product = reportService.GetMostSoldItem();

            reports.Append("The most sold item is: ");
            reports.AppendLine(product.Name);

            return this;
        }

        public IReportsBuilder AddMostExpensiveItemReport()
        {
            Product product = reportService.GetMostExpensiveSoldItem();

            reports.Append("The most expensive item is: ");
            reports.AppendLine(product.Name);

            return this;
        }

        public IReportsBuilder AddTotalNumberOfSoldItemsReport()
        {
            reports.Append("The total number of sold items is: ");
            reports.AppendLine(reportService.GetTotalNumberOfSoldItems().ToString());

            return this;
        }

        public IReportsBuilder AddLeastSoldItemsReport()
        {
            List<Product> products = reportService.GetLeastSoldItems();

            reports.Append("The least sold item(s): ");

            foreach (Product product in products)
            {
                reports.Append($"{product.Name} ");
            }
            reports.AppendLine();

            return this;
        }

        public string GetReports()
        {
            return reports.ToString();
        }
    }
}
