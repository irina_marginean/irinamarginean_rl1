﻿namespace BusinessLogicLayer.Reports
{
    public interface IReportsBuilder
    {
        IReportsBuilder AddMostSoldItemReport();
        IReportsBuilder AddMostExpensiveItemReport();
        IReportsBuilder AddTotalNumberOfSoldItemsReport();
        IReportsBuilder AddLeastSoldItemsReport();

        string GetReports();
    }
}
