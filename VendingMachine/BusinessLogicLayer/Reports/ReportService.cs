﻿using System.Collections.Generic;
using System.Linq;
using BusinessLogicLayer.ProductService;
using BusinessLogicLayer.Transactions;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore.Internal;

namespace BusinessLogicLayer.Reports
{
    public class ReportService : IReportService
    {
        private readonly ITransactionService transactionService;
        private readonly IProductService productService;
        private readonly List<Transaction> transactions;

        public ReportService(ITransactionService transactionService, IProductService productService)
        {
            this.transactionService = transactionService;
            this.productService = productService;
            transactions = transactionService.GetAllTransactions().ToList();
        }

        public int GetTotalNumberOfSoldItems()
        {
            return transactions.Count;
        }

        public Product GetMostExpensiveSoldItem()
        {
            Product product = transactions
                .OrderByDescending(t => t.Product.Price)
                .Select(t => t.Product)
                .FirstOrDefault();

            return product;
        }

        public Product GetMostSoldItem()
        {
            Product product =
                transactions
                    .OrderByDescending(x => transactions
                        .Count(t => t.Product.Guid.Equals(x.Product.Guid)))
                    .Select(trans => trans.Product)
                    .FirstOrDefault();
            
            return product;
        }

        public List<Product> GetLeastSoldItems()
        {
            List<Product> existingProducts = productService.GetProducts().ToList();
            List<Product> soldProducts = transactions.Select(t => t.Product).Distinct().ToList();

            List<Product> notSoldProducts = existingProducts.Except(soldProducts).ToList();

            if (notSoldProducts.Count > 0)
            {
                return notSoldProducts;
            }
            else
            {
                Dictionary<Transaction, int> orderedTransactions =
                    transactions
                        .OrderBy(x => transactions
                            .Count(t => t.Product.Guid.Equals(x.Product.Guid)))
                        .ToDictionary(t => t, t => transactions.Count(trans => trans.Product.Guid.Equals(t.Product.Guid)));

                List<Product> products = orderedTransactions
                    .Where(x => x.Value ==  orderedTransactions.Values.FirstOrDefault())
                    .Select(t => t.Key.Product).Distinct().ToList();

                return products;
            }
        }
    }
}
