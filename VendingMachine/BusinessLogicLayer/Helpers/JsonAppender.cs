﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;

namespace BusinessLogicLayer.Helpers
{
    public static class JsonAppender<T>
    {
        public static void Serialize(string fileName, List<T> entity)
        {
            using StreamWriter file = File.CreateText(fileName);

            JsonSerializer serializer = new JsonSerializer();
            serializer.Serialize(file, entity);
        }

        public static List<T> Deserialize(string fileName)
        {
            var entityData = File.ReadAllText(fileName);

            List<T> entities = JsonConvert.DeserializeObject<List<T>>(entityData);

            return entities;
        }

        public static void AddEntity(string fileName, T entityToAdd)
        {
            List<T> entities;

            try
            {
                entities = Deserialize(fileName);
            }
            catch (FileNotFoundException e)
            {
                entities = new List<T>();
            }
            
            entities.Add(entityToAdd);

            Serialize(fileName, entities);
        }
    }
}
