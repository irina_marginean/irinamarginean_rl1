﻿using System.Collections.Generic;
using BusinessLogicLayer.PaymentService;
using BusinessLogicLayer.ProductService;
using BusinessLogicLayer.Reports;
using BusinessLogicLayer.Transactions;
using DataAccessLayer.Models;

namespace BusinessLogicLayer
{
    public static class VendingMachineFacade
    {
        private static readonly IProductService ProductService = new ProductService.ProductService();
        private static readonly IPaymentStrategy PaymentStrategy = new PaymentStrategy(
        new IPaymentService[]
        {
            new MoneyPayment(), 
            new CardPayment()
        });
        private static readonly ITransactionService TransactionService = new TransactionService();
        private static readonly IReportService ReportService = new ReportService(TransactionService, ProductService);
        private static readonly IReportsBuilder ReportsBuilder = new ReportsBuilder(ReportService);

        public static IEnumerable<Product> GetProducts()
        {
            return ProductService.GetProducts();
        }

        public static void ValidateProduct(Product product)
        {
            ProductService.Validate(product);
        }

        public static void DecreaseProductQuantity(Product product)
        {
            ProductService.DecreaseQuantity(product);
        }

        public static void MakePayment(IPaymentModel paymentModel, decimal valueToPay)
        {
            PaymentStrategy.MakePayment(paymentModel, valueToPay);
        }

        public static void LogTransaction(Transaction transaction)
        {   
            TransactionService.AddTransaction(transaction);
        }

        public static string GenerateReports()
        {
            ReportsBuilder
                .AddTotalNumberOfSoldItemsReport()
                .AddMostExpensiveItemReport()
                .AddMostSoldItemReport()
                .AddLeastSoldItemsReport();

            return ReportsBuilder.GetReports();
        }
        
    }
}
