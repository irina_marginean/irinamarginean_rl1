﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataAccessLayer.Models;
using DataAccessLayer.Repositories;

namespace BusinessLogicLayer.ProductService
{
    public class ProductService : IProductService
    {
        public IEnumerable<Product> GetProducts()
        {
            return RepositoryLocator.ProductRepository.GetAll();
        }

        public void DecreaseQuantity(Product product)
        {
            product.Quantity--;

            RepositoryLocator.ProductRepository.Update(product);
        }

        public void Validate(Product product)
        {
            if (!CanBeSelected(product))
            {
                throw new ValidationException("Not enough products!");
            }
        }

        private bool CanBeSelected(Product product)
        {
            return product.Quantity > 0;
        }
    }
}
