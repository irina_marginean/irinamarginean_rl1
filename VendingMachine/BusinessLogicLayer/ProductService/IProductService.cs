﻿using System.Collections.Generic;
using DataAccessLayer.Models;

namespace BusinessLogicLayer.ProductService
{
    public interface IProductService : IValidate<Product>
    {
        IEnumerable<Product> GetProducts();
        void DecreaseQuantity(Product product);
    }
}
