﻿using System.Collections.Generic;
using System.Linq;
using DataAccessLayer.Models;
using DataAccessLayer.Repositories;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogicLayer.Transactions
{
    class TransactionService : ITransactionService
    {
        public IEnumerable<Transaction> GetAllTransactions()
        {
            IEnumerable<Transaction> transactions = RepositoryLocator.
                                                    TransactionRepository.
                                                    GetQueryable().
                                                    Include(t => t.Product).
                                                    AsEnumerable();
            return transactions;
        }

        public void AddTransaction(Transaction transaction)
        {
            RepositoryLocator.TransactionRepository.Insert(transaction);
        }
    }
}
