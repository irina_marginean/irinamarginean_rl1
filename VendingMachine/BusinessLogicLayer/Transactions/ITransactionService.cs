﻿using System.Collections.Generic;
using DataAccessLayer.Models;

namespace BusinessLogicLayer.Transactions
{
    public interface ITransactionService
    {
        IEnumerable<Transaction> GetAllTransactions();
        void AddTransaction(Transaction transaction);
    }
}
