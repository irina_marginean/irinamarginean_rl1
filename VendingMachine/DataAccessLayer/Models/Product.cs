﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Models
{
    public class Product
    {
        [Key]
        public Guid Guid { get; set; }

        [Required]
        [MaxLength(32)]
        public string Name { get; set; }

        [Required]
        [Range(typeof(decimal), "0", "99", ErrorMessage = "{0} must be a number between {1} and {2}.")]
        public decimal Price { get; set; }

        [Required]
        [Range(0, 10)]
        public int Quantity { get; set; }
    }
}
