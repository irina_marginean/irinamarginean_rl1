﻿namespace DataAccessLayer.Models
{
    public class CardPaymentModel : IPaymentModel
    {
        public string CardHolderName { get; set; }
        public string CardNumber { get; set; }
        public int ExpirationMonth { get; set; }
        public int ExpirationYear { get; set; }
        public int CVV { get; set; }
    }
}
