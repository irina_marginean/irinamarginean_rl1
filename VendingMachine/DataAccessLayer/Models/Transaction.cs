﻿using System;

namespace DataAccessLayer.Models
{
    public class Transaction
    {
        public Guid Id { get; set; }
        public Product Product { get; set; }
        public DateTime Date { get; set; }
        public string PaymentType { get; set; }
    }
}
