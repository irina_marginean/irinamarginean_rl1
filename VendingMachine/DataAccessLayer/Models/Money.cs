﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Models
{
    public class Money
    {
        [Key]
        [Required]
        public string Name { get; set; }

        [Required]
        [Range(typeof(decimal), "0", "100", ErrorMessage = "{0} must be a number between {1} and {2}.")]
        public decimal Value { get; set; }

        [Required]
        [Range(0, Int32.MaxValue)]
        public int Quantity { get; set; }
    }
}
