﻿using System.Collections.Generic;

namespace DataAccessLayer.Models
{
    public class MoneyPaymentModel : IPaymentModel
    {
        public IEnumerable<Money> InputMoney { get; set; }
        public IEnumerable<Money> Rest { get; set; }
    }
}
