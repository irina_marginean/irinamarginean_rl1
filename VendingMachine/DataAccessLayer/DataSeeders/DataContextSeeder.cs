﻿using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.DataSeeders
{
    internal static class DataContextSeeder
    {
        internal static void Seed(ModelBuilder modelBuilder)
        {
            ProductSeeder.Seed(modelBuilder);
            MoneySeeder.Seed(modelBuilder);
        }
    }
}
