﻿using System.Collections.Generic;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.DataSeeders
{
    internal static class MoneySeeder
    {
        internal static List<Money> MoneyList { get; } = GetData();

        internal static void Seed(ModelBuilder modelBuilder)
        {
            foreach (Money money in MoneyList)
            {
                modelBuilder.Entity<Money>().HasData(money);
            }
        }

        private static List<Money> GetData()
        {
            List<Money> moneyList = new List<Money>()
            {
                new Money
                {
                    Name = "OneCent",
                    Value = 0.01M,
                    Quantity = 20
                },
                new Money
                {
                    Name = "FiveCents",
                    Value = 0.05M,
                    Quantity = 10
                },
                new Money
                {
                    Name = "TenCents",
                    Value = 0.1M,
                    Quantity = 15
                },
                new Money
                {
                    Name = "TwentyCents",
                    Value = 0.2M,
                    Quantity = 8
                },
                new Money
                {
                    Name = "FiftyCents",
                    Value = 0.5M,
                    Quantity = 10
                },
                new Money
                {
                    Name = "OneEuro",
                    Value = 1,
                    Quantity = 20
                },
                new Money
                {
                    Name = "TwoEuros",
                    Value = 2,
                    Quantity = 10
                },
                new Money
                {
                    Name = "FiveEuros",
                    Value = 5,
                    Quantity = 10
                },
                new Money
                {
                    Name = "TenEuros",
                    Value = 10,
                    Quantity = 10
                },
                new Money
                {
                    Name = "TwentyEuros",
                    Value = 20,
                    Quantity = 10
                },
                new Money
                {
                    Name = "FiftyEuros",
                    Value = 50,
                    Quantity = 5
                },
                new Money
                {
                    Name = "OneHundredEuros",
                    Value = 100,
                    Quantity = 2
                }
            };

            return moneyList;
        }
    }
}
