﻿using System;
using System.Collections.Generic;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.DataSeeders
{
    internal static class ProductSeeder
    {
        internal static List<Product> Products { get; } = GetData();

        internal static void Seed(ModelBuilder modelBuilder)
        {
            foreach (Product product in Products)
            {
                modelBuilder.Entity<Product>().HasData(product);
            }
        }

        private static List<Product> GetData()
        {
            List<Product> products = new List<Product>()
            {
                new Product
                {
                    Guid = Guid.NewGuid(),
                    Name = "Snickers",
                    Price = new decimal(2.5),
                    Quantity = 4
                },
                new Product
                {
                    Guid = Guid.NewGuid(),
                    Name = "Borsec",
                    Price = new decimal(2.0),
                    Quantity = 8
                },
                new Product
                {
                    Guid = Guid.NewGuid(),
                    Name = "Skittles",
                    Price = new decimal(4.5),
                    Quantity = 2
                },
                new Product
                {
                    Guid = Guid.NewGuid(),
                    Name = "Dorna",
                    Price = new decimal(3.0),
                    Quantity = 3
                },
                new Product
                {
                    Guid = Guid.NewGuid(),
                    Name = "Salam",
                    Price = new decimal(-10.0),
                    Quantity = 6
                }
            };

            return products;
        }
    }
}
