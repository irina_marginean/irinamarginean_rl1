﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccessLayer.Migrations
{
    public partial class AddedTransactions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Guid",
                keyValue: new Guid("35ccd199-c802-4164-9fa3-aeac3b32fb41"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Guid",
                keyValue: new Guid("79ea2d22-4f6d-4cf1-af7e-375827eed61f"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Guid",
                keyValue: new Guid("a4fd531c-89fd-43c1-9bbe-c51eba32bcfa"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Guid",
                keyValue: new Guid("a769e768-2f8e-4c72-8a31-18abfddca69e"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Guid",
                keyValue: new Guid("ad20531a-8d18-4da6-8da9-aac5c5e11e44"));

            migrationBuilder.CreateTable(
                name: "Transactions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ProductGuid = table.Column<Guid>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    PaymentType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Transactions_Products_ProductGuid",
                        column: x => x.ProductGuid,
                        principalTable: "Products",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Guid", "Name", "Price", "Quantity" },
                values: new object[] { new Guid("c2e6bb50-5278-43b1-aa91-473a784c8b3d"), "Snickers", 2.5m, 4 });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Guid", "Name", "Price", "Quantity" },
                values: new object[] { new Guid("03cfee2b-d0e8-4294-adcd-ffafc366e759"), "Borsec", 2m, 8 });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Guid", "Name", "Price", "Quantity" },
                values: new object[] { new Guid("9c14f8fc-e57f-490e-b2d7-cb8f9d96a081"), "Skittles", 4.5m, 2 });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Guid", "Name", "Price", "Quantity" },
                values: new object[] { new Guid("7eea584d-11f7-47e6-93ba-f7f46ab72425"), "Dorna", 3m, 3 });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Guid", "Name", "Price", "Quantity" },
                values: new object[] { new Guid("6cff3db4-9673-4891-b731-e2f823d459a9"), "Salam", -10m, 6 });

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_ProductGuid",
                table: "Transactions",
                column: "ProductGuid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Transactions");

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Guid",
                keyValue: new Guid("03cfee2b-d0e8-4294-adcd-ffafc366e759"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Guid",
                keyValue: new Guid("6cff3db4-9673-4891-b731-e2f823d459a9"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Guid",
                keyValue: new Guid("7eea584d-11f7-47e6-93ba-f7f46ab72425"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Guid",
                keyValue: new Guid("9c14f8fc-e57f-490e-b2d7-cb8f9d96a081"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Guid",
                keyValue: new Guid("c2e6bb50-5278-43b1-aa91-473a784c8b3d"));

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Guid", "Name", "Price", "Quantity" },
                values: new object[] { new Guid("ad20531a-8d18-4da6-8da9-aac5c5e11e44"), "Snickers", 2.5m, 4 });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Guid", "Name", "Price", "Quantity" },
                values: new object[] { new Guid("79ea2d22-4f6d-4cf1-af7e-375827eed61f"), "Borsec", 2m, 8 });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Guid", "Name", "Price", "Quantity" },
                values: new object[] { new Guid("a4fd531c-89fd-43c1-9bbe-c51eba32bcfa"), "Skittles", 4.5m, 2 });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Guid", "Name", "Price", "Quantity" },
                values: new object[] { new Guid("a769e768-2f8e-4c72-8a31-18abfddca69e"), "Dorna", 3m, 3 });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Guid", "Name", "Price", "Quantity" },
                values: new object[] { new Guid("35ccd199-c802-4164-9fa3-aeac3b32fb41"), "Salam", -10m, 6 });
        }
    }
}
