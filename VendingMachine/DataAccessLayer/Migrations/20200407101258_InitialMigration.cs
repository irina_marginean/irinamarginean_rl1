﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccessLayer.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Money",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<decimal>(nullable: false),
                    Quantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Money", x => x.Name);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Guid = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 32, nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    Quantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Guid);
                });

            migrationBuilder.InsertData(
                table: "Money",
                columns: new[] { "Name", "Quantity", "Value" },
                values: new object[] { "OneCent", 20, 0.01m });

            migrationBuilder.InsertData(
                table: "Money",
                columns: new[] { "Name", "Quantity", "Value" },
                values: new object[] { "OneHundredEuros", 2, 100m });

            migrationBuilder.InsertData(
                table: "Money",
                columns: new[] { "Name", "Quantity", "Value" },
                values: new object[] { "FiftyEuros", 5, 50m });

            migrationBuilder.InsertData(
                table: "Money",
                columns: new[] { "Name", "Quantity", "Value" },
                values: new object[] { "TwentyEuros", 10, 20m });

            migrationBuilder.InsertData(
                table: "Money",
                columns: new[] { "Name", "Quantity", "Value" },
                values: new object[] { "FiveEuros", 10, 5m });

            migrationBuilder.InsertData(
                table: "Money",
                columns: new[] { "Name", "Quantity", "Value" },
                values: new object[] { "TwoEuros", 10, 2m });

            migrationBuilder.InsertData(
                table: "Money",
                columns: new[] { "Name", "Quantity", "Value" },
                values: new object[] { "TenEuros", 10, 10m });

            migrationBuilder.InsertData(
                table: "Money",
                columns: new[] { "Name", "Quantity", "Value" },
                values: new object[] { "FiftyCents", 10, 0.5m });

            migrationBuilder.InsertData(
                table: "Money",
                columns: new[] { "Name", "Quantity", "Value" },
                values: new object[] { "TwentyCents", 8, 0.2m });

            migrationBuilder.InsertData(
                table: "Money",
                columns: new[] { "Name", "Quantity", "Value" },
                values: new object[] { "TenCents", 15, 0.1m });

            migrationBuilder.InsertData(
                table: "Money",
                columns: new[] { "Name", "Quantity", "Value" },
                values: new object[] { "FiveCents", 10, 0.05m });

            migrationBuilder.InsertData(
                table: "Money",
                columns: new[] { "Name", "Quantity", "Value" },
                values: new object[] { "OneEuro", 20, 1m });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Guid", "Name", "Price", "Quantity" },
                values: new object[] { new Guid("a769e768-2f8e-4c72-8a31-18abfddca69e"), "Dorna", 3m, 3 });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Guid", "Name", "Price", "Quantity" },
                values: new object[] { new Guid("ad20531a-8d18-4da6-8da9-aac5c5e11e44"), "Snickers", 2.5m, 4 });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Guid", "Name", "Price", "Quantity" },
                values: new object[] { new Guid("79ea2d22-4f6d-4cf1-af7e-375827eed61f"), "Borsec", 2m, 8 });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Guid", "Name", "Price", "Quantity" },
                values: new object[] { new Guid("a4fd531c-89fd-43c1-9bbe-c51eba32bcfa"), "Skittles", 4.5m, 2 });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Guid", "Name", "Price", "Quantity" },
                values: new object[] { new Guid("35ccd199-c802-4164-9fa3-aeac3b32fb41"), "Salam", -10m, 6 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Money");

            migrationBuilder.DropTable(
                name: "Products");
        }
    }
}
