﻿using DataAccessLayer.DataSeeders;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer
{
    public class DataContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Money> Money { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=vending_machine.sqlite");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            DataContextSeeder.Seed(modelBuilder);
        }
    }
}
