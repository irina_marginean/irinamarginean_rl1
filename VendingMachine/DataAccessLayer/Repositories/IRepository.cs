﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Repositories
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetQueryable();
        IEnumerable<T> GetAll();
        void Insert(T entity);
        void Update(T entityToUpdate);
        void Delete(T entityToDelete);
    }
}
