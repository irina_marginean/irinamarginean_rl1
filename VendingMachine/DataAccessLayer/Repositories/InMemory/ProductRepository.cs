﻿using System.Collections.Generic;
using System.Linq;
using DataAccessLayer.DataSeeders;
using DataAccessLayer.Models;

namespace DataAccessLayer.Repositories.InMemory
{
    public class ProductRepository : IRepository<Product>
    {
        private readonly List<Product> products = ProductSeeder.Products;

        public IQueryable<Product> GetQueryable()
        {
            return products.AsQueryable();
        }

        public IEnumerable<Product> GetAll()
        {
            return products.AsEnumerable();
        }

        public void Insert(Product entity)
        {
            products.Add(entity);
        }

        public void Update(Product entityToUpdate)
        {
            Product product = products.FirstOrDefault(x => x.Guid == entityToUpdate.Guid);

            if (product != null)
            {
                product.Name = entityToUpdate.Name;
                product.Quantity = entityToUpdate.Quantity;
                product.Price = entityToUpdate.Price;
            }
        }

        public void Delete(Product entityToDelete)
        {
            products.Remove(entityToDelete);
        }
    }
}
