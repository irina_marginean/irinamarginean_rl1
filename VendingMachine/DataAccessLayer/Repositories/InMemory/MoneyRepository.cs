﻿using System.Collections.Generic;
using System.Linq;
using DataAccessLayer.DataSeeders;
using DataAccessLayer.Models;

namespace DataAccessLayer.Repositories.InMemory
{
    public class MoneyRepository : IRepository<Money>
    {
        private readonly List<Money> moneyList = MoneySeeder.MoneyList;

        public IQueryable<Money> GetQueryable()
        {
            return moneyList.AsQueryable();
        }

        public IEnumerable<Money> GetAll()
        {
            return moneyList.AsEnumerable();
        }

        public void Insert(Money entity)
        {
            moneyList.Add(entity);
        }

        public void Update(Money entityToUpdate)
        {
            Money money = moneyList.FirstOrDefault(x => x.Name == entityToUpdate.Name);

            if (money != null)
            {
                money.Name = entityToUpdate.Name;
                money.Quantity = entityToUpdate.Quantity;
                money.Value = entityToUpdate.Value;
            }
        }

        public void Delete(Money entityToDelete)
        {
            moneyList.Remove(entityToDelete);
        }
    }
}
