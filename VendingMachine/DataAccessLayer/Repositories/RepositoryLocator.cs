﻿using DataAccessLayer.Models;

namespace DataAccessLayer.Repositories
{
    public static class RepositoryLocator
    {
        private static IRepository<Product> productRepository;
        private static IRepository<Money> moneyRepository;
        private static IRepository<Transaction> transactionRepository;
        private static readonly DataContext DataContext = new DataContext();

        public static IRepository<Product> ProductRepository =>
            productRepository ??= new BaseRepository<Product>(DataContext);

        public static IRepository<Money> MoneyRepository =>
            moneyRepository ??= new BaseRepository<Money>(DataContext);

        public static IRepository<Transaction> TransactionRepository =>
            transactionRepository ??= new BaseRepository<Transaction>(DataContext);
    }
}
